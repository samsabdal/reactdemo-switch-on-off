import logo from './logo.svg';
import './App.css';
import {useState} from 'react';
import Demo from './Demo'
function App() {

const [count, setCount]= useState("Hii")
const [btn, setBtn]= useState(false)

const handleClick=() => {
  setCount(count=='Hii' ? 'Hello' : 'Hii');
}
const handleSwitch=() => {
  setBtn(!btn);
}
  return (
    <div className="App">
      <h1>Hello Goutam {count}</h1>
      <Demo x={handleClick}/>
      <button onClick={handleSwitch}>{btn?'ON' : 'OFF'}</button>
    </div>
  );
}

export default App;
